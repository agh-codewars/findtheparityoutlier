public class Kata {

    public static void main(String[] args) {
        System.out.println(new Kata().find(new int[]{160, 3, 1719, 19, 11, 13, -21}));
    }
    
    private int find(int[] nums){

        int sum = Arrays.stream(nums).limit(3).map(i -> Math.abs(i) % 2).sum();
        int mod = (sum == 0 || sum == 1) ? 1 : 0;

        return Arrays.stream(nums).parallel()
                .filter(n -> Math.abs(n) % 2 == mod).findFirst().getAsInt();


        /*switch (detect(nums)) {
            case 0:
                return findOdd(nums);
            case 1:
                return findEven(nums);
        }

        return 0;*/
    }

    /*private int findOdd(int[] nums) {
        return Arrays.stream(nums).filter((i) -> i%2!=0).findFirst().getAsInt();
    }

    private int findEven(int[] nums) {
        return Arrays.stream(nums).filter((i) -> i%2==0).findFirst().getAsInt();
    }

    private int detect(int[] nums) {
        int even = 0;
        int odd = 0;
        for (int i = 0; i < 3; i++) {
            if (nums[i]%2==0)
                even++;
            else odd++;
        }
        return even>odd? 0:1;
    }*/
}